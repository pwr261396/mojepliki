package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.*;

public class Kwadrat extends Figura {
	public Kwadrat(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
	//	int startujx = 0 + rand.nextInt(w - 20);
	//	int startujy = 0 + rand.nextInt(h - 20);
		int startX = 0 + rand.nextInt(w - 20);
		int startY = 0 + rand.nextInt(h - 20);

		shape = new Rectangle2D.Float (startX, startY, 10, 10);
		aft = new AffineTransform();
		area = new Area(shape);

	}

}
