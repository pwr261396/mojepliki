package figury;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;


public class Elipsa extends Figura {
	public Elipsa(Graphics2D buf, int del, int w, int h) {
		super(buf,del,w,h);
		//		int startujx= 0 +rand.nextInt(w-5);
		//		int startujy= 0 +rand.nextInt(h-5);
				int startujx= 20; //+rand.nextInt(w-5);
				int startujy= 30; //+rand.nextInt(h-5);
				shape = new Ellipse2D.Float(startujx, startujy, 10, 10);
				aft= new AffineTransform();
				area = new Area(shape);
				
		
	}
	
}
