package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;

public class Dodatkowafigura extends Figura {

	double xld, xlg, xpg, xpd, xdach, yld, ylg, ypg, ypd, ydach;

	public Dodatkowafigura(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
		GeneralPath domek = new GeneralPath();
		xld = -5;
		yld = -4;
		xlg = -5;
		ylg = 4;
		xpg = 5;
		ypg = 4;
		xpd = 5;
		ypd = -4;
		xdach = 0;
		ydach = 10;

		domek.moveTo(xdach, ydach);
		domek.lineTo(xpg, ypg);
		domek.lineTo(xpd, ypd);
		domek.lineTo(xld, yld);
		domek.lineTo(xlg, ylg);
		domek.lineTo(xdach, ydach);
		domek.closePath();
		shape = domek;
		aft = new AffineTransform();
		area = new Area(shape);
	}
}
